# __Workplace Manager Frontend__

## __Architecture__
****

**Angular 8** was chosen as  framework for developing front-end part of app. Angular 8 is an open-source, client-side TypeScript based JavaScript framework. It is written in TypeScript and complied into JavaScript. Angular 8 is used to create dynamic web applications.
https://angular.io/docs

**npm** was chosen as package manager.
https://www.npmjs.com/

**Material Design** was chosen for adding Material Design components to our Angular app.
https://material.angular.io/

**TSLint** was chosen as a static analysis tool that checks TypeScript code for readability, maintainability, and functionality errors. 
https://palantir.github.io/tslint/

## __Getting Started__
****

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#### Requirements

What you need to run this app:

- ``` node ``` and ```npm```;
- some ```git client```.


#### Installation

**Clone repo:**
```
$ git clone https://gitlab.itechart-group.com/d6.g6/workplace-manager/workplace-manager-frontend.git
```
**Change directory to your app:**
```
$ cd route-to dir
```
**Install the dependencies with npm:**
```
$ npm install
```

#### Running

**Builds and serves app, rebuilding on file changes.**
```
$ ng serve
```
*or*
```
$ npm start
```
You can see build results on: http://localhost:4200/
**Compiles an Angular app into an output directory named dist/ at the given output path. Must be executed from within a workspace directory.**
```
$ ng build
```
*or (in case if you are working in 'webpack' branch)*
```
$ npm run build
```
**Runs unit tests in a project.**
```
$ ng test
```
More info about @agular/cli commands: 
- https://angular.io/cli 
- https://github.com/angular/angular-cli/wiki


## __Notes:__
****
- Do not make changes of *npm  packages version* in __package.json__ file. In case if you need upgrade some package or add new one, please, make sure that everything work correct and doesn't create issues in work of application. Also use stict package version. Don't forget to use ```--save``` or ```--save-dev (-D)``` flags.
- Make sure you have current version of __swagger.yaml__ file in the *root* project folder in case you have some troubles with interfaces in app.