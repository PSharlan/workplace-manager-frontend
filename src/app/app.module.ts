import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { MaterialComponentsModule } from './modules/shared/material-components.module';
import { SharedModule } from './modules/shared/shared.module';
import { AppComponent } from './app.component';
import { AccessTokenInterceptor } from './interceptors/access-token.interceptor';
import { AuthHeaderInterceptor} from './interceptors/auth-header.interceptor';
import { ApiModule } from './generated/api.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    AppRoutingModule,
    SharedModule,
    ApiModule.forRoot({rootUrl: 'http://localhost:8080/api/v1'})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AccessTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
