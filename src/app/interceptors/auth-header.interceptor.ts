import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

import {TokenService} from '../services/token.service';


@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {Authorization: `Bearer ${this.tokenService.getAccessToken()}`}
    });

    if (req.url.includes('svg')) {
      req = req.clone({
        responseType: 'text'
      });
    }
    return next.handle(req);
  }

}
