import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';

import {TokenService} from '../services/token.service';
import {catchError} from 'rxjs/operators';
import {AuthWrapperService} from '../services/auth-wrapper.service';


@Injectable()
export class AccessTokenInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService,
              private authService: AuthWrapperService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!this.tokenService.isTokenExpired()) {
      return next.handle(req);
    }

    if (req.url.includes('login') || req.url.includes('refresh')) {
      return next.handle(req);
    }

    return next.handle(req).pipe(catchError(async () => {
      await this.authService.refreshToken().toPromise().then(success => {
        this.tokenService.saveToken(success);
      });
      return next.handle(req).toPromise();
    }));

  }

}
