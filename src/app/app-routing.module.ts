import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from 'src/app/modules/shared/page-not-found/page-not-found.component';


const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/user/user.module#UserModule',
  },
  {
    path: 'request',
    loadChildren: './modules/request/request.module#RequestModule'
  },
  {
    path: 'search',
    loadChildren: './modules/search/search.module#SearchModule'
  },
  {
    path: 'side-bar',
    loadChildren: './modules/side-bar/side-bar.module#SideBarModule'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
