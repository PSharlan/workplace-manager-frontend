import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {Place} from '../generated/models/place';
import {PlacesService} from '../generated/services/places.service';


@Injectable({
  providedIn: 'root'
})
export class PlacesWrapperService {

  constructor(private placesService: PlacesService) {}

  getByNumberAndByLocationAndByRoomNumber(location: string, roomNumber: string, placeNumber: number): Observable<Place[]> {
    return null;
    // return this.placesService.getAllPlaces(roomNumber, placeNumber, location);
  }

}
