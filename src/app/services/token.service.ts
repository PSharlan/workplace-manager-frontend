import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

import {TokenType} from '../enums/token-type';
import {Token} from '../generated/models/token';


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private jwtHelper: JwtHelperService = new JwtHelperService();

  saveToken(token: Token): void {
    sessionStorage.setItem(TokenType.ACCESS, token.accessToken);
    sessionStorage.setItem(TokenType.REFRESH, token.refreshToken);
  }

  getAccessToken(): string | null {
    return sessionStorage.getItem(TokenType.ACCESS);
  }

  getRefreshToken(): string | null {
    return sessionStorage.getItem(TokenType.REFRESH);
  }

  removeToken(tokenType: TokenType): void {
    sessionStorage.removeItem(tokenType);
  }

  isTokenExpired(): boolean {
    return Boolean((this.getAccessToken() === null) || this.jwtHelper.isTokenExpired(this.getAccessToken()));
  }

}
