import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {Office} from '../generated/models/office';
import {Room} from '../generated/models/room';
import {OfficesService} from '../generated/services/offices.service';


@Injectable({
  providedIn: 'root'
})
export class OfficesWrapperService {

  constructor(private officesService: OfficesService) {}

  getAll(): Observable<Office[]> {
    return this.officesService.getAllOffices();
  }

  getById(id: string): Observable<Room[]> {
    return this.officesService.getOfficeRooms(id);
  }

  createOffice(schemaFile: Blob,
               officeName: string,
               city: string,
               longitude: number = 0,
               latitude: number = 0,
               imageFile: Blob,
               ignoreWarnings: boolean): Observable<Office> {
    return this.officesService.importOfficesFromVsdx(schemaFile, officeName, city, longitude, latitude, imageFile, ignoreWarnings);
  }

  deleteOffice(id: string): Observable<void> {
    return this.officesService.deleteOfficeById(id);
  }

}
