import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {Room} from '../generated/models/room';
import {RoomsService} from '../generated/services/rooms.service';


@Injectable({
  providedIn: 'root'
})
export class RoomsWrapperService {

  constructor(private roomsService: RoomsService) {}

  createRoom(schemaFile: Blob, roomNumber: string, officeId: string) {
    return this.roomsService.importRoomsFromVsdx(schemaFile, roomNumber, officeId);
  }

  updateRoom(schemaFile: Blob, id: string, ) {
    return this.roomsService.updateRoomFromVsdx(schemaFile, id);
  }

  updateRoomAttributes(id: string, managerId: string): Observable<Room> {
    return this.updateRoomAttributes(id, managerId);
  }

  deleteRoom(id: string) {
    return this.roomsService.deleteRoomById(id);
  }

  getById(id: string): Observable<Room> {
    return this.roomsService.getRoomById(id);
  }

  getSvg(id: string): Observable<Blob> {
    return this.roomsService.getRoomSvgById(id);
  }

}
