import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { TokenType } from '../enums/token-type';
import { TokenService } from './token.service';
import {Token} from '../generated/models/token';
import {AuthService} from '../generated/services/auth.service';
import {LoginRequest} from '../generated/models/login-request';


@Injectable({
  providedIn: 'root'
})
export class AuthWrapperService {

  constructor(private http: HttpClient,
              private router: Router,
              private authService: AuthService,
              private tokenService: TokenService) {}

  signin(loginRequest: LoginRequest): Observable<Token> {
    return this.authService.login(loginRequest);
  }

  signout(): void {
    this.tokenService.removeToken(TokenType.ACCESS);
    this.tokenService.removeToken(TokenType.REFRESH);
    this.router.navigate(['signin']).catch(err => console.log(err));
  }

  refreshToken(): Observable<Token> {
    return this.authService.refresh(this.tokenService.getRefreshToken());
  }

}
