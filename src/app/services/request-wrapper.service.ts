import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {CreateRequest} from '../generated/models/create-request';
import {RequestsService} from '../generated/services/requests.service';
import {Request} from '../generated/models/request';


@Injectable({
  providedIn: 'root'
})
export class RequestWrapperService {

  constructor(private requestsService: RequestsService) {}

  create(request: CreateRequest): Observable<Request> {
    return this.requestsService.createRequest(request);
  }

}
