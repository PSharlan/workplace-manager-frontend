import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

import {PageType} from '../enums/page-type';


@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loaderSubscription = new Subject<PageType>();

  showOffices() {
    this.loaderSubscription.next(PageType.OFFICES);
  }

  showSingleOffice() {
    this.loaderSubscription.next(PageType.SINGLE_OFFICE);
  }

  showSingleRoom() {
    this.loaderSubscription.next(PageType.SINGLE_ROOM);
  }

  showEmployeeProfile() {
    this.loaderSubscription.next(PageType.EMPLOYEE_PROFILE);
  }

  showTransferRequest() {
    this.loaderSubscription.next(PageType.TRANSFER_REQUEST);
  }

  showAdminPanel() {
    this.loaderSubscription.next(PageType.ADMIN_PANEL);
  }

  showUserProfile() {
    this.loaderSubscription.next(PageType.USER_PROFILE);
  }

  exite() {
    this.loaderSubscription.next(PageType.USER_PROFILE);
  }


  getSubject(): Observable<PageType> {
    return this.loaderSubscription.asObservable();
  }

}
