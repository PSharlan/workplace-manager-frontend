import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {User} from '../generated/models/user';
import {UsersService} from '../generated/services/users.service';


@Injectable({
  providedIn: 'root'
})
export class UsersWrapperService {

  constructor(private usersService: UsersService) {}

  getCurrent(): Observable<User> {
    return this.usersService.getCurrentUser();
  }

}
