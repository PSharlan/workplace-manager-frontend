import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {Employee} from '../generated/models/employee';
import {Place} from '../generated/models/place';
import {EmployeesService} from '../generated/services/employees.service';


@Injectable({
  providedIn: 'root'
})
export class EmployeesWrapperService {

  constructor(private employeesService: EmployeesService) {}

  getByName(username: string): Observable<Employee[]> {
    return this.employeesService.getAllEmployees(username);
  }

  getById(id: string): Observable<Place> {
    return this.employeesService.getEmployeePlace(id);
  }

}
