import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ErrorStateMatcher } from '@angular/material/core';

import {AuthWrapperService} from '../../../services/auth-wrapper.service';
import {TokenService} from '../../../services/token.service';
import {LoginRequestModel} from '../../../models/login-request.model';
import {Token} from '../../../generated/models/token';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  matcher: ErrorStateMatcher;
  signinForm: FormGroup;
  loginRequest: LoginRequestModel;

  constructor(private authService: AuthWrapperService,
              private tokenService: TokenService,
              private router: Router) {
    this.matcher = new ErrorStateMatcher();
  }

  ngOnInit(): void {
    if (!this.tokenService.isTokenExpired()) {
      this.router.navigate(['']);
    }
    this._createFormGroup();
  }

  private _createFormGroup(): void {
    this.signinForm = new FormGroup({
      username: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
  }

  onSignin(): void {
    if (this.signinForm.valid) {
      const username = this.signinForm.controls.username.value;
      const password = this.signinForm.controls.password.value;
      this.loginRequest = new LoginRequestModel(username, password);
      this.authService.signin(this.loginRequest).subscribe( (token: Token) => {
        this.tokenService.saveToken(token);
        this.router.navigate(['side-bar']);
      }, error => {
        console.log(error);
      });
    }
  }

}
