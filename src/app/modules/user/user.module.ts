import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MaterialComponentsModule } from '../shared/material-components.module';
import { SigninComponent } from './signin/signin.component';


@NgModule({
  declarations: [SigninComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    MaterialComponentsModule
  ]
})
export class UserModule { }
