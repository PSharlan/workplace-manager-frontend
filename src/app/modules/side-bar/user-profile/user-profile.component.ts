import {Component, OnInit} from '@angular/core';

import {UsersWrapperService} from '../../../services/users-wrapper.service';
import {User} from '../../../generated/models/user';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user: User;

  constructor(private userService: UsersWrapperService) {
    this.user = {};
  }

  ngOnInit() {
    this.userService.getCurrent().subscribe((user: User) => {
      this.user = user;
    }, error => {
      console.log(error);
    });
  }

}
