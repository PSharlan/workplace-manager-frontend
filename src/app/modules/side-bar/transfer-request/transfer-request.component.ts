import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {EmployeesWrapperService} from '../../../services/employees-wrapper.service';
import {Office} from '../../../enums/office';
import {PlacesWrapperService} from '../../../services/places-wrapper.service';
import {RequestWrapperService} from '../../../services/request-wrapper.service';
import {CreateRequestDetailsModel} from '../../../models/create-request-details.model';
import {CreateRequestModel} from '../../../models/create-request.model';
import {RequestDetailsType} from '../../../generated/models/request-details-type';
import {Place} from '../../../generated/models/place';
import {Employee} from '../../../generated/models/employee';

const PIPE_SYMBOL = '|';
const PIPE_CODE = '%7C';
const ROOM_PATTERN = '[0-9]{3,}';
const PLACE_PATTERN = '[0-9]{1,}';
const NAME_PATTERN = '[a-zA-Z-]{2,}';
const SUCCESSFUL_REQUEST_MSG = 'Transfer request has been sent successful!';


@Component({
  selector: 'app-transfer-request',
  templateUrl: './transfer-request.component.html',
  styleUrls: ['./transfer-request.component.scss']
})
export class TransferRequestComponent {

  firstName: string;
  lastName: string;
  message: string;

  requestTypes = RequestDetailsType;
  offices = Office;

  targetEmployeePlace: Place[];
  currentEmployeePlace: Place;
  employees: Employee[];
  selectedEmployee: FormControl;
  employeeForm: FormGroup;
  placeForm: FormGroup;

  constructor(private employeesWrapperService: EmployeesWrapperService,
              private placesWrapperService: PlacesWrapperService,
              private requestWrapperService: RequestWrapperService) {
    this.targetEmployeePlace = [];
    this.currentEmployeePlace = {};
    this.selectedEmployee = new FormControl('', Validators.required);
    this.placeForm = new FormGroup({
      office: new FormControl(),
      room: new FormControl('', [
        Validators.required,
        Validators.pattern(ROOM_PATTERN)
      ]),
      place: new FormControl('', [
        Validators.required,
        Validators.pattern(PLACE_PATTERN)
      ])
    });
    this.employeeForm = new FormGroup({
      requestType: new FormControl('', [
        Validators.required
      ]),
      firstName: new FormControl('', [
        Validators.required,
        Validators.pattern(NAME_PATTERN)
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.pattern(NAME_PATTERN)
      ])
    });
  }

  searchEmployee() {
    this.firstName = this.employeeForm.controls.firstName.value;
    this.lastName = this.employeeForm.controls.lastName.value;

    const username = this.lastName + ' ' + this.firstName;

    this.employeesWrapperService.getByName(username).subscribe((employees: Employee[]) => {
      this.employees = employees;
    }, error => {
      console.log(error);
    });
  }

  getEmployeePlace() {
    this.employeesWrapperService.getById(this.selectedEmployee.value.id).subscribe((employee: Employee) => {
      this.currentEmployeePlace = employee;
    }, error => {
      console.log(error);
    });
  }

  getTargetPlace() {
    const office = this.placeForm.controls.office.value.value;
    const location = office.replace(PIPE_SYMBOL, PIPE_CODE);
    const roomNumber = this.placeForm.controls.room.value;
    const placeNumber = +this.placeForm.controls.place.value;

    this.placesWrapperService.getByNumberAndByLocationAndByRoomNumber(location, roomNumber, placeNumber)
      .subscribe((places: Place[]) => {
        this.targetEmployeePlace = places;
        this.sendTransferRequest();
    }, error => {
        console.log(error);
    });
  }

  sendTransferRequest() {
    const type = this.employeeForm.controls.requestType.value.value;
    const currentPlace = this.currentEmployeePlace;
    const targetPlace = this.targetEmployeePlace;
    const detail = new CreateRequestDetailsModel();

    if (type !== RequestDetailsType.REMOVE && targetPlace != null) {
      detail.placeToId = targetPlace[0].id;
    }
    if (type !== RequestDetailsType.PLACE && currentPlace != null) {
      detail.placeFromId = currentPlace.id;
    }
    detail.employeeId = this.selectedEmployee.value.id;
    detail.type = type;

    const request = new CreateRequestModel();
    request.details.push(detail);

    this.requestWrapperService.create(request).subscribe(() => {
      this.message = SUCCESSFUL_REQUEST_MSG;
    }, error => {
      this.message = error.error.message;
    });
  }

}
