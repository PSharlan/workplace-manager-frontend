import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatDialogModule, MatExpansionModule,
  MatCheckboxModule,
  MatIconModule,
  MatInputModule, MatRadioModule, MatSelectModule, MatSlideToggleModule,
  MatStepperModule,
  MatTooltipModule, MatTabsModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SideBarRoutingModule} from './side-bar-routing.module';

import {MainPipe} from '../../helpers/main-pipe.module';

import {WrapperComponent} from './wrapper/wrapper.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {OfficesComponent} from './offices/offices.component';
import {SingleOfficeComponent} from './single-office/single-office.component';
import {EmployeeProfileComponent} from './employee-profile/employee-profile.component';
import {SingleRoomComponent} from './single-room/single-room.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {TransferRequestComponent} from './transfer-request/transfer-request.component';
import {UpdateRoomAttributesDialogComponent} from './single-room/update-room-attributes/update-room-attributes-dialog';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { CreateOfficeComponent } from './admin-panel/create-office/create-office.component';
import { CreateRoomComponent } from './admin-panel/create-room/create-room.component';
import { UpdateRoomComponent } from './admin-panel/update-room/update-room.component';
import { UploadFileComponent } from './admin-panel/upload-file/upload-file.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { DeleteRoomComponent } from './admin-panel/delete-room/delete-room.component';
import { SuccessfulResponseComponent } from './admin-panel/successful-response/successful-response.component';
import { ErrorResponseComponent } from './admin-panel/error-response/error-response.component';
import { DeleteOfficeComponent } from './admin-panel/delete-office/delete-office.component';


@NgModule({
  declarations: [
    WrapperComponent,
    NavBarComponent,
    OfficesComponent,
    SingleOfficeComponent,
    EmployeeProfileComponent,
    SingleRoomComponent,
    TransferRequestComponent,
    UserProfileComponent,
    UpdateRoomAttributesDialogComponent,
    CreateOfficeComponent,
    CreateRoomComponent,
    UpdateRoomComponent,
    UploadFileComponent,
    AdminPanelComponent,
    DeleteRoomComponent,
    SuccessfulResponseComponent,
    ErrorResponseComponent,
    DeleteOfficeComponent
  ],
  imports: [
    CommonModule,
    SideBarRoutingModule,
    MatIconModule,
    MatTooltipModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatRadioModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatBottomSheetModule,
    ReactiveFormsModule,
    FormsModule,
    MainPipe,
    DragDropModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    DragDropModule,
    MatExpansionModule,
    MatDialogModule,
    MatTabsModule
  ],
  entryComponents: [
    UpdateRoomAttributesDialogComponent
  ],
  providers: [{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
})
export class SideBarModule { }
