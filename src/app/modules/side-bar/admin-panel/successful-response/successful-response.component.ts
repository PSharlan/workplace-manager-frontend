import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-successful-response',
  templateUrl: './successful-response.component.html',
  styleUrls: ['./successful-response.component.scss']
})
export class SuccessfulResponseComponent {

  @Input() successfulResponse: boolean;

}
