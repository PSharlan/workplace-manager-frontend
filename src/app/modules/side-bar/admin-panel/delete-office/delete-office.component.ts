import { Component, OnInit } from '@angular/core';
import {Office} from '../../../../generated/models/office';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Problem} from '../../../../generated/models/problem';
import {OfficesWrapperService} from '../../../../services/offices-wrapper.service';

@Component({
  selector: 'app-delete-office',
  templateUrl: './delete-office.component.html',
  styleUrls: ['./delete-office.component.scss']
})
export class DeleteOfficeComponent implements OnInit {

  invalidRoomForDelete: boolean;
  isUploadedSuccessful: boolean;

  offices: Office[];
  deleteOfficeForm: FormGroup;
  problem: Problem;

  constructor(private officesWrapperService: OfficesWrapperService) {
    this.invalidRoomForDelete = false;
    this.isUploadedSuccessful = false;
    this.offices = [];
    this.deleteOfficeForm = new FormGroup({
      officeId: new FormControl('', [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.officesWrapperService.getAll().subscribe((offices: Office[]) => {
      this.offices = offices;
    }, error => {
      console.log(error);
    });
  }

  deleteOffice(): void {
    this.closeMessage();
    const officeId = this.deleteOfficeForm.controls.officeId.value;
    this.officesWrapperService.deleteOffice(officeId).subscribe(() => {
      this.isUploadedSuccessful = true;
    }, error => {
      this.problem = error.error;
    });
  }

  closeMessage() {
    this.problem = null;
    this.isUploadedSuccessful = false;
  }

}
