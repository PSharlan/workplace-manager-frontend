import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {Office} from '../../../../generated/models/office';
import {Room} from '../../../../generated/models/room';
import {Problem} from '../../../../generated/models/problem';
import {OfficesWrapperService} from '../../../../services/offices-wrapper.service';
import {RoomsWrapperService} from '../../../../services/rooms-wrapper.service';

const ROOM_NUMBER_PATTERN = '^[0-9]{1,}';


@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss']
})
export class CreateRoomComponent implements OnInit {

  inputRoomNumberDisable: boolean;
  creationRoomDuplication: boolean;
  isUploadedSuccessful: boolean;

  offices: Office[];
  rooms: Room[];
  uploadedFile: File;
  problem: Problem;
  createRoomForm: FormGroup;

  constructor(private officesWrapperService: OfficesWrapperService,
              private roomsWrapperService: RoomsWrapperService) {
    this.inputRoomNumberDisable = true;
    this.creationRoomDuplication = false;
    this.isUploadedSuccessful = false;
    this.offices = [];
    this.rooms = [];
    this.createRoomForm = new FormGroup({
      officeId: new FormControl('', [
        Validators.required
      ]),
      roomNumber: new FormControl({value: '', disabled: true}, [
        Validators.required,
        Validators.pattern(ROOM_NUMBER_PATTERN)
      ])
    });
  }

  ngOnInit(): void {
    this.officesWrapperService.getAll().subscribe((offices: Office[]) => {
      this.offices = offices;
    }, error => {
      console.log(error);
    });

  }

  getRoomsByOfficeId(): void {
    this.officesWrapperService.getById(this.createRoomForm.controls.officeId.value).subscribe((rooms: Room[]) => {
      this.rooms = rooms;
      this.createRoomForm.controls.roomNumber.enable();
    }, error => {
      console.log(error.error);
    });
  }

  checkRoomForDuplication(): void {
    this.creationRoomDuplication = false;
    this.rooms.forEach(room => {
      if (room.number === this.createRoomForm.controls.roomNumber.value) {
        this.creationRoomDuplication = true;
      }
    });
  }

  createRoom(): void {
    this.closeMessage();
    const officeId = this.createRoomForm.controls.officeId.value;
    const roomNumber = this.createRoomForm.controls.roomNumber.value;
    this.roomsWrapperService.createRoom(this.uploadedFile, roomNumber, officeId).subscribe(() => {
      this.isUploadedSuccessful = true;
    }, error => {
      this.problem = error.error;
    });
  }

  uploadFileStart(): void {
    this.uploadedFile = null;
    this.closeMessage();
  }

  closeMessage() {
    this.isUploadedSuccessful = false;
    this.problem = null;
  }

  getFile(file: File) {
    this.uploadedFile = file;
  }

}
