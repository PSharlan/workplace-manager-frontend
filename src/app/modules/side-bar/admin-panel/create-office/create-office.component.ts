import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {Problem} from '../../../../generated/models/problem';
import {OfficesWrapperService} from '../../../../services/offices-wrapper.service';

const CITY_PATTERN = '[A-Za-z\- ]{1,}';
const SHORT_DESIGNATION_PATTERN = '[A-Z]{1,2}[0-9]{1,}';


@Component({
  selector: 'app-create-office',
  templateUrl: './create-office.component.html',
  styleUrls: ['./create-office.component.scss']
})
export class CreateOfficeComponent {

  ignoreWarnings: boolean;
  isUploadedSuccessful: boolean;

  createOfficeForm: FormGroup;
  uploadedVsdFile: File;
  uploadedImgFile: File;
  problem: Problem;

  constructor(private officesWrapperService: OfficesWrapperService) {
    this.ignoreWarnings = false;
    this.isUploadedSuccessful = false;
    this.createOfficeForm = new FormGroup({
      city: new FormControl('', [
        Validators.required,
        Validators.pattern(CITY_PATTERN)
      ]),
      shortDesignation: new FormControl('', [
        Validators.required,
        Validators.pattern(SHORT_DESIGNATION_PATTERN)
      ]),
      longitude: new FormControl(0),
      latitude: new FormControl(0),
    });
  }

  sendUploadedFile(): void {
    this.closeMessage();
    this.officesWrapperService.createOffice(
      this.uploadedVsdFile,
      this.createOfficeForm.controls.shortDesignation.value,
      this.createOfficeForm.controls.city.value,
      this.createOfficeForm.controls.longitude.value,
      this.createOfficeForm.controls.latitude.value,
      this.uploadedImgFile,
      this.ignoreWarnings).subscribe(() => {
        this.isUploadedSuccessful = true;
      }, error => {
         this.problem = error.error;
      });
  }

  uploadFileStart(): void {
    this.ignoreWarnings = false;
    this.closeMessage();
  }

  closeMessage(): void {
    this.problem = null;
    this.isUploadedSuccessful = false;
  }

  getVsdFile(file: File): void {
    this.closeMessage();
    this.uploadedVsdFile = file;
  }

  removeVsdFile(): void {
    this.ignoreWarnings = false;
    this.uploadedVsdFile = null;
    this.closeMessage();
  }

  getImgFile(file: File): void {
    this.uploadedImgFile = file;
  }

  removeImgFile(): void {
    this.uploadedImgFile = null;
  }

}
