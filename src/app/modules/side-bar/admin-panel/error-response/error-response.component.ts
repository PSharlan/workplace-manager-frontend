import {Component, Input, OnInit} from '@angular/core';
import {Problem} from '../../../../generated/models/problem';

const RESOLVED_ISSUES_COLOR = '#008000';
const UNRESOLVED_ISSUES_COLOR = '#646871b8';
const UNRESOLVED_ISSUE_CLASS = 'unsuccessful-message';
const RESOLVED_ISSUE_CLASS = 'resolved-issue';

@Component({
  selector: 'app-error-response',
  templateUrl: './error-response.component.html',
  styleUrls: ['./error-response.component.scss']
})
export class ErrorResponseComponent {

  resolvedIssues: number;
  resolvedIssuesColor: string;

  @Input() problem: Problem;

  constructor() {
    this.resolvedIssuesColor = UNRESOLVED_ISSUES_COLOR;
    this.resolvedIssues = 0;
  }

  resolveIssue(event): void {
    const checkedIssue = event.source._elementRef.nativeElement.parentElement.classList;
    if (event.checked) {
      checkedIssue.replace(UNRESOLVED_ISSUE_CLASS, RESOLVED_ISSUE_CLASS);
      this.resolvedIssues++;
    } else {
      checkedIssue.replace(RESOLVED_ISSUE_CLASS, UNRESOLVED_ISSUE_CLASS);
      this.resolvedIssues--;
    }
    if (this.resolvedIssues === this.problem.violations.length) {
      this.resolvedIssuesColor = RESOLVED_ISSUES_COLOR;
    } else {
      this.resolvedIssuesColor = UNRESOLVED_ISSUES_COLOR;
    }
  }

}
