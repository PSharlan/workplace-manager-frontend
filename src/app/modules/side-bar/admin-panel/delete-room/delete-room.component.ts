import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {Office} from '../../../../generated/models/office';
import {Room} from '../../../../generated/models/room';
import {Problem} from '../../../../generated/models/problem';
import {OfficesWrapperService} from '../../../../services/offices-wrapper.service';
import {RoomsWrapperService} from '../../../../services/rooms-wrapper.service';


@Component({
  selector: 'app-delete-room',
  templateUrl: './delete-room.component.html',
  styleUrls: ['./delete-room.component.scss']
})
export class DeleteRoomComponent implements OnInit {

  invalidRoomForDelete: boolean;
  isUploadedSuccessful: boolean;

  offices: Office[];
  rooms: Room[];
  deleteRoomForm: FormGroup;
  problem: Problem;

  constructor(private officesWrapperService: OfficesWrapperService,
              private roomWrapperService: RoomsWrapperService) {
    this.invalidRoomForDelete = false;
    this.isUploadedSuccessful = false;
    this.offices = [];
    this.rooms = [];
    this.deleteRoomForm = new FormGroup({
      officeId: new FormControl('', [
        Validators.required
      ]),
      roomNumber: new FormControl({value: '', disabled: true}, [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.officesWrapperService.getAll().subscribe((offices: Office[]) => {
      this.offices = offices;
    }, error => {
      console.log(error);
    });
  }

  getRoomsByOffice(): void {
    this.deleteRoomForm.controls.roomNumber.enable();
    this.officesWrapperService.getById(this.deleteRoomForm.controls.officeId.value).subscribe((rooms: Room[]) => {
      this.rooms = rooms;
      this.deleteRoomForm.controls.roomNumber.enable();
    }, error => {
      console.log(error);
    });
  }

  validateRoom(): void {
    this.invalidRoomForDelete = false;
    const selectedRoom = this.deleteRoomForm.controls.roomNumber.value;
    if (selectedRoom.totalPlaces !== selectedRoom.freePlaces) {
      this.invalidRoomForDelete = true;
    }
  }

  deleteRoom(): void {
    this.closeMessage();
    const roomId = this.deleteRoomForm.controls.roomNumber.value.id;
    this.roomWrapperService.deleteRoom(roomId).subscribe(() => {
      this.isUploadedSuccessful = true;
    }, error => {
      this.problem = error.error;
    });
  }

  closeMessage(): void {
    this.problem = null;
    this.isUploadedSuccessful = false;
  }

}
