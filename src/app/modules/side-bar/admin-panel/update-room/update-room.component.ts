import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {Office} from '../../../../generated/models/office';
import {Room} from '../../../../generated/models/room';
import {Problem} from '../../../../generated/models/problem';
import {OfficesWrapperService} from '../../../../services/offices-wrapper.service';
import {RoomsWrapperService} from '../../../../services/rooms-wrapper.service';


@Component({
  selector: 'app-update-room',
  templateUrl: './update-room.component.html',
  styleUrls: ['./update-room.component.scss']
})
export class UpdateRoomComponent implements OnInit {

  invalidRoomForUpdate: boolean;
  isUploadedSuccessful: boolean;

  offices: Office[];
  rooms: Room[];
  uploadedFile: File;
  updateRoomForm: FormGroup;
  problem: Problem;

  constructor(private officesWrapperService: OfficesWrapperService,
              private roomsWrapperService: RoomsWrapperService) {
    this.invalidRoomForUpdate = false;
    this.isUploadedSuccessful = false;
    this.offices = [];
    this.rooms = [];

    this.updateRoomForm = new FormGroup({
      officeId: new FormControl('', [
        Validators.required
      ]),
      roomNumber: new FormControl({value: '', disabled: true}, [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.officesWrapperService.getAll().subscribe((offices: Office[]) => {
      this.offices = offices;
    }, error => {
      console.log(error);
    });
  }

  getRoomsByOffice(): void {
    this.updateRoomForm.controls.roomNumber.enable();
    this.officesWrapperService.getById(this.updateRoomForm.controls.officeId.value).subscribe((rooms: Room[]) => {
      this.rooms = rooms;
      this.updateRoomForm.controls.roomNumber.enable();
    }, error => {
      console.log(error);
    });
  }

  validateRoom(): void {
    this.invalidRoomForUpdate = false;
    const selectedRoom = this.updateRoomForm.controls.roomNumber.value;
    if (selectedRoom.totalPlaces !== selectedRoom.freePlaces) {
      this.invalidRoomForUpdate = true;
    }
  }

  updateRoom(): void {
    this.closeMessage();
    const roomId = this.updateRoomForm.controls.roomNumber.value.id;
    this.roomsWrapperService.updateRoom(this.uploadedFile, roomId).subscribe(() => {
      this.isUploadedSuccessful = true;
    }, error => {
      this.problem = error.error;
    } );
  }

  uploadFileStart(): void {
    this.uploadedFile = null;
    this.closeMessage();
  }

  closeMessage(): void {
    this.isUploadedSuccessful = false;
    this.problem = null;
  }

  getFile(file: File): void {
    this.uploadedFile = file;
  }

}
