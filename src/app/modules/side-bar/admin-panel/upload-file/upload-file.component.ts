import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent {

  uploadedFile: File;

  @Output() file = new EventEmitter<File>();
  @Output() uploadFileStart = new EventEmitter();

  @Input() buttonName: string;

  uploadFile(file: any): void {
    this.uploadedFile = null;
    this.uploadFileStart.emit();
    this.uploadedFile = file.target.files[0];
    this.file.emit(this.uploadedFile);
  }

}
