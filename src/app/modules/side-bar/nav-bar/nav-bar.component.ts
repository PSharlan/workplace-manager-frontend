import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {LoaderService } from '../../../services/loader.service';
import {PageType} from '../../../enums/page-type';
import {AuthWrapperService} from '../../../services/auth-wrapper.service';
import {Office} from '../../../generated/models/office';
import {Room} from '../../../generated/models/room';
import {Employee} from '../../../generated/models/employee';

const DEFAULT_NAV_BAR_WIDTH = '360px';
const TRANSFER_REQUEST_NAV_BAR_WIDTH = '460px';
const ADMIN_PANEL_NAV_BAR_WIDTH = '50%';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private cdr: ChangeDetectorRef,
              private loaderService: LoaderService,
              private authService: AuthWrapperService) {}

  loading = PageType.OFFICES;
  subscription: Subscription;

  panelWidth = DEFAULT_NAV_BAR_WIDTH;

  office: Office;
  room: Room;
  employee: Employee;

  ngOnInit() {
    this.subscription = this.loaderService.getSubject().subscribe(
      loadingState => {
        this.loading = loadingState;
        this.cdr.detectChanges();
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showUserProfile() {
   this.loaderService.showUserProfile();
   this.panelWidth = DEFAULT_NAV_BAR_WIDTH;
  }

  showOffices() {
    this.loaderService.showOffices();
    this.panelWidth = DEFAULT_NAV_BAR_WIDTH;
  }

  showTransferRequest() {
    this.loaderService.showTransferRequest();
    this.panelWidth = TRANSFER_REQUEST_NAV_BAR_WIDTH;
  }

  showAdminPanel() {
    this.loaderService.showAdminPanel();
    this.panelWidth = ADMIN_PANEL_NAV_BAR_WIDTH;
  }

  getOffice(office: Office) {
    this.office = office;
  }

  getRoom(room: Room) {
    this.room = room;
  }

  getEmployee(employee: Employee) {
    this.employee = employee;
  }

  exit() {
    this.authService.signout();
    this.router.navigate(['signin']);
  }

}
