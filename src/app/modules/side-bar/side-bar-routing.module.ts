import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WrapperComponent} from './wrapper/wrapper.component';
import {AuthGuard} from '../../guards/auth.guard';


const routes: Routes = [
  {
    path: 'wrapper',
    component: WrapperComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    redirectTo: 'wrapper'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SideBarRoutingModule { }
