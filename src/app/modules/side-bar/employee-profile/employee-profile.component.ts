import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

import {LoaderService} from '../../../services/loader.service';
import {Employee} from '../../../generated/models/employee';


@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.scss']
})
export class EmployeeProfileComponent {

  constructor(private router: Router,
              private loaderService: LoaderService) {}

  @Input() employee: Employee;

  redirectBackToSingleRoom() {
    this.loaderService.showSingleRoom();
  }

}
