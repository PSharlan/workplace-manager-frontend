import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {FormControl} from '@angular/forms';

import {UpdateRoomAttributesDialogComponent} from './update-room-attributes/update-room-attributes-dialog';
import {LoaderService} from '../../../services/loader.service';
import {Room} from '../../../generated/models/room';
import {Place} from '../../../generated/models/place';
import {Problem} from '../../../generated/models/problem';
import {Employee} from '../../../generated/models/employee';
import {RoomsWrapperService} from '../../../services/rooms-wrapper.service';

enum TableColor {
  available = 'rgba(57, 153, 82, 0.3)',
  unavailable = 'rgba(227, 43, 43, 0.3)'
}

enum SvgIdentifier {
  roomClass = 'svg-room',
  tableClass = 'svg-table',
  tableIdPrefix = 'svg-table-',
  tablesTextClass = 'svg-table-text',
  tableTextIdPrefix = 'svg-table-text-'
}

const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
const LABEL_HEIGHT_MULTIPLIER = 4;
const SHORT_NAME_POSTFIX = '...';


@Component({
  selector: 'app-single-room',
  templateUrl: './single-room.component.html',
  styleUrls: ['./single-room.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SingleRoomComponent implements OnInit {

  svg: string;
  currentZoom: number;
  zoomStep: number;
  mouseOnLabel: boolean;

  search: FormControl;
  currentRoom: Room;
  employees: Employee[];
  shownEmployees: Employee[];

  @Input() roomId: string;
  @Output() employee = new EventEmitter<Employee>();

  constructor(private dialog: MatDialog,
              private router: Router,
              private loaderService: LoaderService,
              private roomsWrapperService: RoomsWrapperService) {
    this.currentRoom = {};
    this.employees = [];
    this.shownEmployees = [];
    this.currentZoom = 1;
    this.zoomStep = 0.25;
    this.search = new FormControl('');
  }

  ngOnInit(): void {
    this.roomsWrapperService.getById(this.roomId).subscribe((room: Room) => {
      this.currentRoom = room;
      this.employees = this.getEmployee(this.currentRoom.places);
      this.shownEmployees = this.sortEmployees(this.employees);
      this.getInnerSvg();
    }, (error: Problem) => {
      console.log(error);
    });
  }

  getEmployee(places: Place[]): Employee[] {
    const employees = [];
    for (const place of places) {
      if (place.employee !== undefined) {
        employees.push(place.employee);
      }
    }
    return employees;
  }

  sortEmployees(employees: Employee[]): Employee[] {
    return employees.sort((a, b) => a.name > b.name ? 1 : -1);
  }

  getInnerSvg(): void {
    this.roomsWrapperService.getSvg(this.roomId).subscribe((svg: Blob) => {
      this.svg = String(svg);
      const image = document.getElementsByClassName(SvgIdentifier.roomClass).item(0);
      image.innerHTML = this.svg;
      this.markTable();
    }, (error: Problem) => {
      console.log(error);
    });
  }

  markTable(): void {
    const tables = document.getElementsByClassName(SvgIdentifier.tableClass);
    const tableNames = document.getElementsByClassName(SvgIdentifier.tablesTextClass);

    for (const place of this.currentRoom.places) {
      const table = tables.namedItem(SvgIdentifier.tableIdPrefix + place.number.toString());
      const text = tableNames.namedItem(SvgIdentifier.tableTextIdPrefix + place.number.toString());
      const label = text.parentElement;
      const fontSize = parseFloat(text.attributes.getNamedItem('font-size').value);
      if (place.employee == null) {
        table.attributes.getNamedItem('fill').value = TableColor.available;
      } else {
        table.attributes.getNamedItem('fill').value = TableColor.unavailable;

        label.addEventListener('mouseenter', this.onHoverZoomIn.bind(this));
        label.addEventListener('mouseleave', this.onHoverZoomOut.bind(this));
        label.addEventListener('click', this.showEmployeeProfile.bind(this));

        const tspan = text.firstElementChild;
        tspan.textContent = place.employee.department.name;

        const rect = label.firstElementChild;
        const initialLabelWidth = parseFloat(rect.attributes.getNamedItem('width').value);
        const initialLabelHeight = parseFloat(rect.attributes.getNamedItem('height').value);
        const initialLabelX = parseFloat(rect.attributes.getNamedItem('x').value);
        const initialLabelY = parseFloat(rect.attributes.getNamedItem('y').value);

        // Replace text with '...' if it's too long
        const len = Math.round(initialLabelWidth / fontSize * 1.4);
        const name = place.employee.name;
        text.textContent = this.prepareText(name, len);
        text.appendChild(tspan);

        // Create an image tag and add to the label. It's gonna be invisible by default
        const imageSize = initialLabelHeight * (LABEL_HEIGHT_MULTIPLIER - 1.5);
        const tlX = initialLabelX + (initialLabelWidth - imageSize) / 2;
        const margin = imageSize * 0.15;
        const tlY = initialLabelY - (initialLabelHeight * (LABEL_HEIGHT_MULTIPLIER - 1)) + margin;
        const image = this.createSvgImage(tlX, tlY, place.employee.imageLink, imageSize);
        label.appendChild(image);
      }

      const svg = document.getElementsByClassName(SvgIdentifier.roomClass).item(0);
      svg.attributes.setNamedItem(this.createAttr('style', `transform: scale(${this.currentZoom})`));
    }
  }

  createSvgImage(tlX: number, tlY: number, href: string, size: number): SVGImageElement {
    const image = document.createElementNS(SVG_NAMESPACE, 'image');
    image.attributes.setNamedItem(this.createAttr('x', String(tlX)));
    image.attributes.setNamedItem(this.createAttr('y', String(tlY)));
    image.attributes.setNamedItem(this.createAttr('href',  href));
    image.attributes.setNamedItem(this.createAttr('height', String(size)));
    image.attributes.setNamedItem(this.createAttr('width', String(size)));
    image.attributes.setNamedItem(this.createAttr('visibility', 'hidden'));
    return image;
  }

  createAttr(name: string, value: string): Attr {
    const attr = document.createAttribute(name);
    attr.value = value;
    return attr;
  }

  onHoverZoomIn($event): void {
    if (this.mouseOnLabel) {
      return;
    }
    this.mouseOnLabel = true;

    // Move current <g> to the end of DOM
    const label = $event.currentTarget;
    const tableGroup = label.parentElement;
    const svg = tableGroup.parentElement;
    const last = svg.lastChild;
    last.parentNode.insertBefore(tableGroup, last.nextSibling);

    const rect = label.children[0];
    const text = label.children[1];
    const tspan = text.firstElementChild;

    const fontSize = parseFloat(text.attributes.getNamedItem('font-size').value);
    const currentWidth = parseFloat(rect.attributes.getNamedItem('width').value);
    const currentHeight = parseFloat(rect.attributes.getNamedItem('height').value);
    const currentX = parseFloat(rect.attributes.getNamedItem('x').value);
    const currentY = parseFloat(rect.attributes.getNamedItem('y').value);

    const newHeight = currentHeight * LABEL_HEIGHT_MULTIPLIER;
    const newWidth = newHeight * 1.2;
    const newX = currentX - (newWidth - currentWidth) / 2;
    const newY = currentY - (newHeight - currentHeight);

    rect.attributes.getNamedItem('width').value = newWidth;
    rect.attributes.getNamedItem('height').value = newHeight;
    rect.attributes.getNamedItem('x').value = newX;
    rect.attributes.getNamedItem('y').value = newY;

    const len = Math.round(newWidth / fontSize * 1.6);
    const name = this.findNameByTextId(text.id);
    text.textContent = this.prepareText(name, len);
    text.appendChild(tspan);

    const image = label.children[2];
    image.attributes.getNamedItem('visibility').value = 'visible';
  }

  onHoverZoomOut($event): void {
    this.mouseOnLabel = false;
    const label = $event.currentTarget;
    const tableGroup = label.parentElement;
    const table = tableGroup.firstElementChild;
    const rect = label.children[0];
    const text = label.children[1];
    const tspan = text.firstElementChild;

    const fontSize = parseFloat(text.attributes.getNamedItem('font-size').value);
    const currentWidth = parseFloat(rect.attributes.getNamedItem('width').value);
    const currentHeight = parseFloat(rect.attributes.getNamedItem('height').value);
    const currentX = parseFloat(rect.attributes.getNamedItem('x').value);
    const currentY = parseFloat(rect.attributes.getNamedItem('y').value);
    const tableWidth = parseFloat(table.attributes.getNamedItem('width').value);

    const newHeight = currentHeight / LABEL_HEIGHT_MULTIPLIER;
    const newWidth = tableWidth;
    const newX = currentX + (currentWidth - newWidth) / 2;
    const newY = currentY + (currentHeight - newHeight);

    rect.attributes.getNamedItem('width').value = newWidth;
    rect.attributes.getNamedItem('height').value = newHeight;
    rect.attributes.getNamedItem('x').value = newX;
    rect.attributes.getNamedItem('y').value = newY;

    const len = Math.round(newWidth / fontSize * 1.4);
    const name = this.findNameByTextId(text.id);
    text.textContent = this.prepareText(name, len);
    text.appendChild(tspan);

    const image = label.children[2];
    image.attributes.getNamedItem('visibility').value = 'hidden';
  }

  findNameByTextId(textId: string): string {
    for (const place of this.currentRoom.places) {
      if (SvgIdentifier.tableTextIdPrefix + place.number.toString() === textId) {
        return place.employee.name;
      }
    }
  }

  prepareText(name: string, maxLength: number): string {
    if (maxLength + 2 < name.length) {
      return name.substring(0, maxLength) + SHORT_NAME_POSTFIX;
    }
    return name;
  }

  showEmployeeProfile($event): void {
    const text = $event.currentTarget.children[1];
    for (const place of this.currentRoom.places) {
      if (SvgIdentifier.tableTextIdPrefix + place.number.toString() === text.id) {
        this.redirectToEmployeeProfile(place.employee);
      }
    }
  }

  zoom(num): void {
    this.currentZoom *= num;
    const svg = document.getElementsByClassName(SvgIdentifier.roomClass).item(0);
    svg.attributes.getNamedItem('style').textContent = `transform: scale(${this.currentZoom})`;
  }

  searchEmployee(): void {
    const searchInputValue = this.search.value.trim().toLowerCase();
    this.shownEmployees = [];
    for (const employee of this.employees) {
      const name = employee.name.toLowerCase();
      const localizedName =  employee.localizedName.toLowerCase();

      if (name.includes(searchInputValue) || localizedName.includes(searchInputValue)) {
        this.shownEmployees.push(employee);
        continue;
      }
      if (searchInputValue.includes(' ')) {
        const revertName = this.revertEmployeeName(name);
        const revertLocalizedName = this.revertEmployeeName(localizedName);

        if (revertName.includes(searchInputValue) || revertLocalizedName.includes(searchInputValue)) {
          this.shownEmployees.push(employee);
        }
      }
    }
  }

  revertEmployeeName(fullName: string): string {
    const nameStartIndex = fullName.lastIndexOf(' ');
    const firstName = fullName.substring(nameStartIndex).trim();
    const lastName = fullName.substring(0, nameStartIndex).trim();
    return firstName + ' ' + lastName;
  }

  redirectToEmployeeProfile(employee: Employee): void {
    this.employee.emit(employee);
    this.loaderService.showEmployeeProfile();
  }

  redirectToSingleOffice(): void {
    this.loaderService.showSingleOffice();
  }

  editRoomAttributes() {
    const dialogRef = this.dialog.open(UpdateRoomAttributesDialogComponent, {
      width: '500px',
      data: this.currentRoom
    });

    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit();
    });

  }

}
