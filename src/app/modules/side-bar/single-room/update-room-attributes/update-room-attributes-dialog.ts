import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {EmployeesWrapperService} from '../../../../services/employees-wrapper.service';
import {Employee} from '../../../../generated/models/employee';
import {Room} from '../../../../generated/models/room';
import {RoomsWrapperService} from '../../../../services/rooms-wrapper.service';

const NAME_PATTERN = '[A-Z]{1}[a-z]{1,}';
const UPDATE_DM_SUCCESSFUL_MESSAGE = 'Room attributes has been updated successful!';
const UPDATE_DM_UNSUCCESSFUL_MESSAGE = 'Something goes wrong.. Try again!';


@Component({
  selector: 'app-update-room-attributes-dialog',
  templateUrl: './update-room-attributes-dialog.html',
  styleUrls: ['./update-room-attributes-dialog.scss'],
})
export class UpdateRoomAttributesDialogComponent {

  successUpdate: boolean;
  unsuccessUpdate: boolean;
  message: string;

  employees: Employee[];
  updatedRoom: Room;
  managerForm: FormGroup;
  selectedEmployee: FormControl;

  constructor(private dialogRef: MatDialogRef<UpdateRoomAttributesDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Room,
              private employeeService: EmployeesWrapperService,
              private roomService: RoomsWrapperService) {
    this.successUpdate = false;
    this.unsuccessUpdate = false;
    this.managerForm = new FormGroup({
      lastName: new FormControl('', [
        Validators.required,
        Validators.pattern(NAME_PATTERN)
      ]),
      firstName: new FormControl('', [
        Validators.required,
        Validators.pattern(NAME_PATTERN)
      ])
    });
    this.selectedEmployee = new FormControl('', Validators.required);
  }

  searchManager(): void {
    const firstName = this.managerForm.controls.firstName.value;
    const lastName = this.managerForm.controls.lastName.value;
    const username = lastName + ' ' + firstName;

    this.employeeService.getByName(username).subscribe((employees: Employee[]) => {
      this.employees = employees;
    }, error => {
      console.log(error);
    });
  }

  updateRoomAttributes(): void {
    this.successUpdate = false;
    this.unsuccessUpdate = false;
    this.roomService.updateRoomAttributes(this.data.id, this.selectedEmployee.value.id).subscribe((room: Room) => {
      this.updatedRoom = room;
      this.message = UPDATE_DM_SUCCESSFUL_MESSAGE;
      this.successUpdate = true;
    }, error => {
      this.message = UPDATE_DM_UNSUCCESSFUL_MESSAGE;
      this.unsuccessUpdate = true;
      console.log(error);
    });
  }

  cnClose(): void {
    this.dialogRef.close();
  }

}
