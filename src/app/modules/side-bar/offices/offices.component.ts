import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {LoaderService} from '../../../services/loader.service';
import {Office} from '../../../generated/models/office';
import {OfficesWrapperService} from '../../../services/offices-wrapper.service';


@Component({
  selector: 'app-offices',
  templateUrl: './offices.component.html',
  styleUrls: ['./offices.component.scss']
})
export class OfficesComponent implements OnInit {

  constructor(private router: Router,
              private officesWrapperService: OfficesWrapperService,
              private loaderService: LoaderService) {}

  @Output() office = new EventEmitter<Office>();

  offices: Office[] = [];

  ngOnInit() {
    this.officesWrapperService.getAll().subscribe((offices: Office[]) => {
      this.offices = offices;
    }, error => {
      console.log(error);
    });
  }

  redirectToOffice(office: Office) {
    this.office.emit(office);
    this.loaderService.showSingleOffice();
  }

}
