import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {LoaderService} from '../../../services/loader.service';
import {Room} from '../../../generated/models/room';
import {Office} from '../../../generated/models/office';
import {OfficesWrapperService} from '../../../services/offices-wrapper.service';


@Component({
  selector: 'app-single-office',
  templateUrl: './single-office.component.html',
  styleUrls: ['./single-office.component.scss']
})
export class SingleOfficeComponent implements OnInit {

  constructor(private router: Router,
              private loaderService: LoaderService,
              private officesWrapperService: OfficesWrapperService) {}

  @Input() office: Office;
  @Output() room = new EventEmitter<Room>();

  roomMap = new Map();
  rooms: Room[] = [];

  ngOnInit() {
    this.officesWrapperService.getById(this.office.id).subscribe((rooms: Room[]) => {
      this.rooms = rooms;
      this.getRoomMap(this.rooms);
    }, error => {
      console.log(error);
    });
  }

  getRoomMap(rooms: Room[]) {
    rooms.map((room) => {
      if (this.roomMap.has(room.floorNumber)) {
        const item = this.roomMap.get(room.floorNumber);
        item.push(room);
      } else {
        this.roomMap.set(room.floorNumber, [room]);
      }
    });
  }

  redirectToRoom(room: Room) {
    this.room.emit(room);
    this.loaderService.showSingleRoom();
  }

  redirectToOffices() {
    this.loaderService.showOffices();
  }

}
