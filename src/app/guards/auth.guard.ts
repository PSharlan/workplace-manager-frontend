import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from '@angular/router';

import { TokenService } from '../services/token.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private isTokenExpired: boolean;
  private accessToken: string | null;

  constructor(private tokenService: TokenService, private router: Router) {
    this.isTokenExpired = this.tokenService.isTokenExpired();
    this.accessToken = this.tokenService.getAccessToken();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.accessToken === null) {
      this.router.navigate(['signin']);
      return false;
    }
    return true;
  }

}
