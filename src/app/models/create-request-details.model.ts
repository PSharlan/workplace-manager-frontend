import {CreateRequestDetails} from '../generated/models/create-request-details';
import {RequestDetailsType} from '../generated/models/request-details-type';


export class CreateRequestDetailsModel implements CreateRequestDetails {

  employeeId: string;
  placeFromId: string;
  placeToId: string;
  type: RequestDetailsType;

}
