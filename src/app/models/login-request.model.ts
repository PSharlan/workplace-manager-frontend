import {LoginRequest} from '../generated/models/login-request';


export class LoginRequestModel implements LoginRequest {

  password: string;
  username: string;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }

}
