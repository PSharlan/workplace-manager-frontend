import {CreateRequest} from '../generated/models/create-request';
import {CreateRequestDetails} from '../generated/models/create-request-details';


export class CreateRequestModel implements CreateRequest {

  details = new Array<CreateRequestDetails>();

}
