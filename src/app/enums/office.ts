export enum Office {
  BREST = 'Brest | M275',
  VITEBSK_K30 = 'Vitebsk | K30',
  VITEBSK_K16 = 'Vitebsk | K16',
  POLOTSK = 'Polotsk | N17',
  GOMEL = 'Gomel | K3',
  GRODNO = 'Grodno | V2',
  MOGILEV = 'Mogilev | L9',
  MINSK_T8 = 'Minsk | T8',
  MINSK_T10 = 'Minsk | T10',
  MINSK_P49 = 'Minsk | P49'
}
